#include "../../utils/common.h"
#include "experimental_data.C"

enum comparison { all, theory, hadrons }; //what do we want to compare our results to?
TString comp_suff[]={"","_theory","_hadrons"};

void RAA_RCP(C_systems system=cent, double pTlead=5.0, int binning=1,TString evo="GPC2", TString label="", bool ppBias=0, comparison compareRAA=all, TString ext="pdf")
/*compareRAA:
 all - show hadron RAA and jet RAA models in one plot, 
 hadrons - show only hadrons RAA, not the theoretical models, 
 theory - show only theoretical models, not the hadron RAA*/
{
	if(ppBias==1 && ((pTlead>3.0 && pTlead<5.0)||(pTlead>5.0 && pTlead<7.0))) return; //we have pythia histograms only for pTlead 3, 5 and 7 GeV
	
	//setings
	bool removeFirstBin=1; //do we want to remove the first datapoint (both for RCP and RAA) due to the bad closure test?
	
	//RAA
	bool showRAA=1; //show RAA plot at all
	bool showKrishnaAll=0; //show all 3 variants of hybrid model calculation
	bool showVitev=1; //show Ivan's RAA calculation
	bool showKrishna=1; //show Krishna's RAA calculation
		bool wake=1; //parameter of Krishna's calculation
		bool wake_positive_only=0; //parameter of Krishna's calculation
	bool showLIDO=1; //show LIDO theory calculation
	bool showLBT=1; //show LBT theory calculation
	
	bool showAliceRAA=0; //show Alice full jet RAA
	bool showPhenixhRAAchh=1; //show PHENIX's charged hadron RAA
	bool showPhenixhRAApi0=1; //show PHENIX's pi0 RAA
	bool showSTARRAAchh=1; //show STAR's charged hadron RAA
	bool plotLogX_RAA=0; //log x-axis
	bool plotLogY_RAA=1; //log y-axis
	bool drawBiasLine=0; //mark unbiased region
	
	if(showKrishnaAll) //show all 3 variants of Krishna's calculation 
	{
		showKrishna=0;
	}
	if(compareRAA==hadrons) //show only hadrons RAA, not the theoretical models
	{
		showKrishnaAll=0;
		showVitev=0;
		showKrishna=0;
		plotLogX_RAA=1;
	}
	else if(compareRAA==theory) //show only theoretical models, not the hadron RAA
	{
		showAliceRAA=0;
		showPhenixhRAAchh=0;
		showPhenixhRAApi0=0;
		showSTARRAAchh=0;
	}
	
	//RCP
	bool showRCP=1; //show RCP plot at all
	bool showAlex=0; //show Alex's h+jet I_CP
	bool showAliceRCP=1; //show Alice RCP
	bool showSTARhRCP=1; //show STAR's charged hadron RCP
	bool showAtlasRCP=1; //show ATLAS ch. hadron RCP
	bool plotLogX_RCP=1; //log x-axis
	bool plotLogY_RCP=1; //log y-axis

	if(ppBias || system==peri)showRCP=0; //show only RAA since RCP is the same as with ppBias=0
	if(system==peri)
	{
		showVitev=0; 
	 showKrishna=0; 
	}
	//Colors
	Color_t cstar=kBlue; //STAR data
	Color_t cgray=kGray+1; //STAR data in biased region
	Color_t cstarH=kAzure+1; //star hadron RCP
	Color_t cstarJ=kCyan+1; //star jet norm. unc.
	Color_t calice=kRed; //ALICE
	Color_t catlas=kRed-9; //ATLAS
	Color_t calex=kBlue+1; //STAR h+jet
	Color_t cphx=kMagenta-6; //PHENIX
	Color_t ckrsh=kOrange-8; //Krishna's HYBRID model
		Color_t ckrsh1=kMagenta+2; //Krishna's HYBRID model
		Color_t ckrsh2=kViolet-9; //Krishna's HYBRID model
		Color_t ckrsh3=kOrange-8; //Krishna's HYBRID model
	Color_t ctheory_vit=kCyan-2; //Ivan's SCET model
	Color_t ctheory_vitNLO=kAzure-7; //Ivan's NLO
	Color_t clido=kOrange+9; //LIDO theory calculation
	Color_t clbt=kMagenta; //LBT theory calculation
	
	//line at unity
	Color_t lineOneColor=kGray;
	int lineOneWidth=1;
	int lineOneStyle=2;
	
	//pTlead bias line
	Color_t lineBiasColor=kRed-6;
	int lineBiasWidth=2;
	float xbiasLine[C_nR]={14,16,16}; //x position of a line showing pTlead bias range in RAA
	float ybiasLine=1.0;//y range of a line showing pTlead bias range in RAA
	float xbiasDesc[C_nR]={0.52,0.52,0.52}; //x position of the bias textbox
	float ybiasDesc=0.26; //x position of the bias textbox
	
	//descriptions
	float G_latex_sz=0.032; //label text size
	Color_t G_latex_cl=kGray+1; //label color
	
	//text size
	float RCP_legTextSize=0.045; //RCP legends

	//markers
	int markstar=29;
	int msizestar=4;
	int markalice=20;
	int msizealice=2;
	int markatlasH=24;
	int msizeatlasH=2;
	int markphx=27;
	int markphx2=24;
	int msizephx=2;
	int marklbt=28;
	int msizelbt=1;
	int markstarH=30;
	int msizestarH=3;
	int phxFill=1001; //phenix fill style
	int krshFill=1001; //Krishna's RAA fill style
		int krshFill1=1001; //Krishna's RAA fill style
		int krshFill2=1001; //Krishna's RAA fill style
		int krshFill3=1001; //Krishna's RAA fill style
	int lidoFill=3475; //LIDO RAA fill style
	
	//axis ranges
	//RAA
	float xmaxRAA=47;
	float xminRAA=(plotLogX_RAA)?4.0:0.0;
	float yminRAA=(plotLogY_RAA)? 0.05:0;
	float ymaxRAA=(plotLogY_RAA)?10.0:1.0; //2.0:0.8
	
	float xshift=(removeFirstBin)? 1.0 : 0.0; //used to remove the first datapoint
	float ptminRAA_STAR=pTlead+xshift;//STAR data range
	float ptmaxRAA_STAR=30; //STAR data range
	
	//RCP
	float xminRCP=0.05;
	float xmaxRCP=120;
	float xmaxRCP_short=40; //in case we don't show LHC results
	if(plotLogX_RCP) 
	{
		xminRCP=0.2;
		xmaxRCP=150;
		xmaxRCP_short=100; 
	}

	float yminRCP=(plotLogY_RCP)? 0.05:0.05;
	float ymaxRCP=(plotLogY_RCP)?2.0:1.3;
	float ptminRCP_STAR=pTlead+xshift;//STAR data range
	float ptmaxRCP_STAR=25; //STAR data range

	gROOT->LoadMacro("./util.C");
	gROOT->LoadMacro("./Utility.C");
   gStyle->SetOptStat(0);
	
	TString outDir=Form("../../../plotting_out/obr/%s/results/comparison/",evo.Data());
	TString sufPPbias[2]={"","_ppBiased"};
	
	TCanvas *cfig1=new TCanvas("cfig1","fig1",10,10,1400,500);
	cfig1->Divide(C_nR,1,0,0);
	
	//Alice
	double alxRAA[3]={45,55,65};
	double alxRAA_err[3]={5,5,5};
	double alyRAA[3]={0.293,0.282,0.266};
	double alyRAA_err_shape[3]={0.015,0.01,0.015};
	double alyRAA_err_corr[3]={0.035,0.032,0.035};
	double alyRAA_err[3];
	for(int i=0;i<3;i++)
	{
		alyRAA_err[i]=TMath::Sqrt(alyRAA_err_corr[i]*alyRAA_err_corr[i]+alyRAA_err_shape[i]*alyRAA_err_shape[i]);
	}
	TGraphErrors* galiceRAA=new TGraphErrors(3,alxRAA,alyRAA,alxRAA_err,0);
	TGraphErrors* galiceRAA_sys=new TGraphErrors(3,alxRAA,alyRAA,alxRAA_err,alyRAA_err);

	//Phenix charged hadrons RAA
	TGraphAsymmErrors *gphxRAAchhad[2];
	TGraphAsymmErrors *gphxRAAchhad_sys[2];
	gphxRAAchhad[0]=new TGraphAsymmErrors(31,phxRAAchhad_x_0to10,phxRAAchhad_y_0to10,0,0,phxRAAchhad_y_stat_0to10,phxRAAchhad_y_stat_0to10);
	gphxRAAchhad[1]=new TGraphAsymmErrors(30,phxRAAchhad_x_60to80,phxRAAchhad_y_60to80,0,0,phxRAAchhad_y_stat_60to80,phxRAAchhad_y_stat_60to80);
	gphxRAAchhad_sys[0]=new TGraphAsymmErrors(31,phxRAAchhad_x_0to10,phxRAAchhad_y_0to10,0,0,phxRAAchhad_y_sysDown_0to10,phxRAAchhad_y_sysUp_0to10);
	gphxRAAchhad_sys[1]=new TGraphAsymmErrors(30,phxRAAchhad_x_60to80,phxRAAchhad_y_60to80,0,0,phxRAAchhad_y_sysDown_60to80,phxRAAchhad_y_sysUp_60to80);
	

	//Phenix pi0 RAA from arXiv:1208.2254 
	TGraphErrors *gphxRAApi0[2];
	TGraphErrors *gphxRAApi0_sys[2];
	gphxRAApi0[0]=new TGraphErrors(15,phxRAApi0_x_0to10,phxRAApi0_y_0to10,0,phxRAApi0_y_stat_0to10);
	gphxRAApi0_sys[0]=new TGraphErrors(15,phxRAApi0_x_0to10,phxRAApi0_y_0to10,0,phxRAApi0_y_sys_0to10);
	gphxRAApi0[1]=new TGraphErrors(14,phxRAApi0_x_60to80,phxRAApi0_y_60to80,0,phxRAApi0_y_stat_60to80);
	gphxRAApi0_sys[1]=new TGraphErrors(14,phxRAApi0_x_60to80,phxRAApi0_y_60to80,0,phxRAApi0_y_sys_60to80);
	
	//TGraphAsymmErrors *gphxRAA=new TGraphAsymmErrors(5,phxRAAchhad_x,phxRAAchhad_y_center,phxRAAchhad_x_err,phxRAAchhad_x_err,phxRAAchhad_y_low,phxRAAchhad_y_high);
	//TGraph *gphxRAAchhad_sys=graph_band(5,phxRAAchhad_x,phxRAAchhad_y_high,phxRAAchhad_y_low,50);
	
	//STAR charged hadrons RAA
	TGraphErrors *gstrRAA[3];
	gstrRAA[0]=new TGraphErrors(18,strRAA_x,strRAA_y_0to5,0,strRAA_y_0to5_err);
	gstrRAA[1]=new TGraphErrors(18,strRAA_x,strRAA_y_60to80,0,strRAA_y_60to80_err);
	
   //STAR h+jet Icp
	TGraph *gIcp[C_nR];
	TGraph *gIcp_sys[C_nR];
	gIcp[0]=new TGraph(9,icp_R02_x,icp_R02_y_center);
	gIcp[1]=new TGraph(11,icp_R03_x,icp_R03_y_center);
	gIcp_sys[0]=graph_band(9,icp_R02_x,icp_R02_y_high,icp_R02_y_low,50);
	gIcp_sys[1]=graph_band(11,icp_R03_x,icp_R03_y_high,icp_R03_y_low,50);
	
	//ATLAS ch. hadrons RCP 	arXiv:1504.04337
	const int nATLAShRCPbins=35;
	TGraphErrors* gatlashRCP=new TGraphErrors(nATLAShRCPbins,atlashRCP_x,atlashRCP_y,0,atlashRCP_stat);
	TGraphErrors* gatlashRCP_sys=new TGraphErrors(nATLAShRCPbins,atlashRCP_x,atlashRCP_y,atlashRCP_xw,atlashRCP_sys);
	
	//Alice RCP (arXiv:1311.0633)
	TGraphErrors* galiceRCP[C_nR];
	TGraphAsymmErrors* galiceRCP_sys[C_nR];
	const int nAliceRCPbins=7;
	galiceRCP[0]=new TGraphErrors(nAliceRCPbins,alxRCP02,alyRCP02,alxRCP02_err,alyRCP02_err);
	galiceRCP_sys[0]=new TGraphAsymmErrors(nAliceRCPbins,alxRCP02,alyRCP02,alxRCP02_err,alxRCP02_err,alyRCP02_syserr_up,alyRCP02_syserr_down);
	galiceRCP[1]=new TGraphErrors(nAliceRCPbins-2,alxRCP03,alyRCP03,alxRCP03_err,alyRCP03_err);
	galiceRCP_sys[1]=new TGraphAsymmErrors(nAliceRCPbins-2,alxRCP03,alyRCP03,alxRCP02_err,alxRCP02_err,alyRCP03_syserr_up,alyRCP03_syserr_down);
	
	//STAR hadron RCP (arxiv:nucl-ex/0305015)
	const int nhRCPbins=35;
	TGraphErrors* gSTARhRCP=new TGraphErrors(nhRCPbins,hRCPx,hRCPy,0,hRCPerr);
	
	//declare arrays
	TFile* fp[C_nR];

	//SCET
	TGraphErrors *gVitRAA_g22[C_nR];
	TGraphErrors *gVitRAA_g20[C_nR];
	TGraphErrors *gVitRAA_g22b[C_nR];
	TGraphErrors *gVitRAA_g20b[C_nR];
	TGraphErrors *gVitRAA_NLO_down[C_nR];
	TGraphErrors *gVitRAA_NLO_up[C_nR];
			
	//HYBRID model
	TGraphErrors *gKrishnaRAA[C_nR];
	TGraphErrors *gKrishnaRAA1[C_nR];
	TGraphErrors *gKrishnaRAA2[C_nR];
	TGraphErrors *gKrishnaRAA3[C_nR];

	//LIDO
	TGraphErrors *gLIDORAA[C_nR];
	TGraphErrors *gLIDORAA_biasedAA[C_nR];
	
	//LBT
	TGraphErrors *gLBTRAA[C_nR];
	
	TGraphAsymmErrors *gjanRAA[C_nR];
	TGraphAsymmErrors *gjanRAA_sys[C_nR];
	TGraphAsymmErrors *gjanRAA_gray[C_nR]; //gray data points in biased region
	TGraphAsymmErrors *gjanRAA_sys_gray[C_nR]; //gray data points in biased region
	TGraphAsymmErrors *gjanRCP[C_nR];
	TGraphAsymmErrors *gjanRCP_sys[C_nR];
	TH1D* htmp1[C_nR];
	TH1D* htmp2[C_nR];
	
   TLatex *latex = new TLatex();
   latex->SetNDC();
   latex->SetTextSize(0.05);
	float Rxpos[C_nR]={0.75,0.15,0.15};
	float Rxpos2[C_nR]={0.7,0.4,0.5};
	float shrinkTheory[C_nR]={40,40,40};
	
	float infoXmin,infoYmin,infoXmax,infoYmax;
	float posx1,posy1,posx2,posy2;
	
	TLine *one = new TLine(xminRAA, 1, xmaxRAA,1);
	one->SetLineWidth(lineOneWidth);
	one->SetLineStyle(lineOneStyle);
	one->SetLineColor(lineOneColor);
	
	//STAR hadron normalization error
	double boxX=xminRAA+9*(xmaxRAA-xminRAA)/10;
	TBox* ppboxStr=new TBox(boxX-2, 1-normErrStr[system], boxX-1, 1+normErrStr[system]);
	ppboxStr->SetFillColor(cstarH);
	ppboxStr->SetFillStyle(1000);
	
	//PHENIX hadron normalization error
	TBox* ppboxPhx=new TBox(boxX-4, 1-normErrPhx[system], boxX-3, 1+normErrPhx[system]);
	ppboxPhx->SetFillColor(cphx);
	ppboxPhx->SetFillStyle(1000);
	
	//STAR jet normalization error
	TBox* ppboxStrJet=new TBox(boxX, 1-normErrStrJet[system], boxX+1, 1+normErrStrJet[system]);
	ppboxStrJet->SetFillColor(cstarJ);
	ppboxStrJet->SetFillStyle(1000);
	

	//********************
	//RAA
	//********************
	if(showRAA){
	cout<<"plotting RAA"<<endl;
	//loop over R
	for(int r=0; r<C_nR; r++)
	{

	float R=C_Rs[r];
	bool plotAlice=(R<0.25) ? true : false; //we have Alice results only for R=0.2

	//---------------------------------------------------------------------------
  //Vitev
	double cronin1=1.5;
	double eloss1=1.0;
	double cronin2=1.0;
	double eloss2=1.5;
	Color_t cvit1=ctheory_vit; //line color
	Color_t cvit2=ctheory_vit;
	Color_t cvit3=ctheory_vitNLO;
	int lsvit1=1; //line style
	int lsvit2=1;
	int lsvit3=3;
	int lwvit1=1; //line width
	int lwvit2=1;
	int lwvit3=1; 
	
	//SCET model
	gVitRAA_g22[r]=gReadData(Form("theory/vitev/AuAu_cron%.1lf_eloss%.1lf_200GeV_g2.2_R%0.lf.txt",cronin1,eloss1,R*10),1,0);
	gVitRAA_g20[r]=gReadData(Form("theory/vitev/AuAu_cron%.1lf_eloss%.1lf_200GeV_g2.0_R%0.lf.txt",cronin1,eloss1,R*10),1,0);
	
	gVitRAA_g22b[r]=gReadData(Form("theory/vitev/AuAu_cron%.1lf_eloss%.1lf_200GeV_g2.2_R%0.lf.txt",cronin2,eloss2,R*10),1,0);
	gVitRAA_g20b[r]=gReadData(Form("theory/vitev/AuAu_cron%.1lf_eloss%.1lf_200GeV_g2.0_R%0.lf.txt",cronin2,eloss2,R*10),1,0);
	
	gVitRAA_g20[r]->SetLineColor(cvit1);
	gVitRAA_g20[r]->SetLineStyle(lsvit1);
	gVitRAA_g20[r]->SetLineWidth(lwvit1);
	gVitRAA_g20b[r]->SetLineColor(cvit2);
	gVitRAA_g20b[r]->SetLineStyle(lsvit2);
	gVitRAA_g20b[r]->SetLineWidth(lwvit2);
	
	//NLO 
	if(r!=1)
	{
		gVitRAA_NLO_up[r]=gReadData(Form("theory/vitev/Raa_NLO-Au-Wmin0.R%.1lfcoldUP.dat",R),1,0);
		gVitRAA_NLO_down[r]=gReadData(Form("theory/vitev/Raa_NLO-Au-Wmin0.R%.1lfcoldDN.dat",R),1,0);
	
		gVitRAA_NLO_down[r]->SetLineColor(cvit3);
		gVitRAA_NLO_down[r]->SetLineStyle(lsvit3);
		gVitRAA_NLO_down[r]->SetLineWidth(lwvit3);
	}
	
	//Krishna 
	//gKrishnaRAA[r]=gReadData3(Form("theory/hybrid_results_2017/cent010jetRAA_R0%.0lf.dat",R*10),5);
	gKrishnaRAA[r]=gReadData4(Form("theory/hybrid_results_2019/010_RAA_R%.0lf_wake_%i_ignore_neg_%i.dat",R*10,wake,wake_positive_only),5);
	if(showKrishnaAll)
	{
		gKrishnaRAA1[r]=gReadData4(Form("theory/hybrid_results_2019/010_RAA_R%.0lf_wake_0_ignore_neg_0.dat",R*10),5);
		gKrishnaRAA2[r]=gReadData4(Form("theory/hybrid_results_2019/010_RAA_R%.0lf_wake_1_ignore_neg_1.dat",R*10),5);
		gKrishnaRAA3[r]=gReadData4(Form("theory/hybrid_results_2019/010_RAA_R%.0lf_wake_1_ignore_neg_0.dat",R*10),5);
	}
	
	//LIDO
	gLIDORAA[r]=gReadData5(Form("theory/LIDO/LIDO-AuAu-200GeV/0-10/0d%.0lf/jetRaa-unbiased.dat",R*10));
	gLIDORAA_biasedAA[r]=gReadData5(Form("theory/LIDO/LIDO-AuAu-200GeV/0-10/0d%.0lf/jetRaa-biased-AA.dat",R*10));
	
	//LBT
	gLBTRAA[r]=gReadData6(Form("theory/LBT/predictionofjetraaatauau200gev/RAA_R%.0lf.txt",R*10));
	
	//Jan
	TString indir=Form("%s%s",evo.Data(),sufPPbias[ppBias].Data());
	fp[r]=new TFile(Form("../../../plotting_out/systematics/%s/%s/bining%i/systematics_R%.1lf_pT%0.lf.root",C_system_name[system].Data(),indir.Data(),binning,R,pTlead),"read");
	TGraphAsymmErrors *gjanRAA_unf=(TGraphAsymmErrors*) fp[r]->Get("RAA_unfold_BSL");
	TGraphAsymmErrors *gjanRAA_corr=(TGraphAsymmErrors*) fp[r]->Get("RAA_corr_BSL");
	gjanRAA_sys[r]=gcombine_syserr(gjanRAA_unf,gjanRAA_corr,1,1);
	gjanRAA[r]=(TGraphAsymmErrors*) fp[r]->Get("RAA_BSL");

	gjanRAA_sys_gray[r]=(TGraphAsymmErrors*) gjanRAA_sys[r]->Clone();
	gjanRAA_gray[r]=(TGraphAsymmErrors*) gjanRAA[r]->Clone();
	
	//shrink graphs
	double shirnk_x=xbiasLine[r];
	if(ppBias==1) shirnk_x=ptminRAA_STAR;
	ShrinkGraph(gjanRAA[r],shirnk_x,ptmaxRAA_STAR);
	ShrinkGraph(gjanRAA_sys[r],shirnk_x,ptmaxRAA_STAR);
	ShrinkGraph(gjanRAA_gray[r],ptminRAA_STAR,shirnk_x);
	ShrinkGraph(gjanRAA_sys_gray[r],ptminRAA_STAR,shirnk_x);
	ShrinkGraph(gKrishnaRAA[r],pTlead,shrinkTheory[r]);
	if(showKrishnaAll)
	{
		ShrinkGraph(gKrishnaRAA1[r],pTlead,shrinkTheory[r]);
		ShrinkGraph(gKrishnaRAA2[r],pTlead,shrinkTheory[r]);
		ShrinkGraph(gKrishnaRAA3[r],pTlead,shrinkTheory[r]);
	}

	ShrinkGraph(gLIDORAA[r],pTlead+4,shrinkTheory[r]+5);
	ShrinkGraph(gLBTRAA[r],pTlead+5,shrinkTheory[r]);
	

	TLatex *latexL = new TLatex();
	latexL->SetNDC();
	latexL->SetTextSize(G_latex_sz);
	latexL->SetTextColor(G_latex_cl);
	
	//********************
	//Draw plots
	//********************
  TString xtitle="";
  if(r==C_nR-1) 
  {
	  xtitle="#it{p}_{T, jet}^{ch}, #it{p}_{T, jet} (GeV/#it{c})";
	  if(compareRAA==hadrons) xtitle="#it{p}_{T, jet}^{ch}, #it{p}_{T} (GeV/#it{c})";
  }
  //if(!plotAlice) xtitle="#it{p}_{T, jet}^{ch} [GeV/#it{c}]";
  cfig1->cd(r+1);
  gPad->SetMargin(0.20,0.03,0.20,0.03);
  if(r==0) gPad->SetRightMargin(0);
  if(r==1){
	  gPad->SetLeftMargin(0);
	  gPad->SetRightMargin(0);
	  xminRAA=xminRAA+0.1;}
  if(r==2) gPad->SetLeftMargin(0);
  gPad->SetTicks(1);
  if(plotLogY_RAA) gPad->SetLogy();
  if(plotLogX_RAA) gPad->SetLogx();
  htmp1[r]=new TH1D(Form("htmp1_%i",r),"",100,xminRAA,xmaxRAA);
  htmp1[r]->SetXTitle(xtitle);htmp1[r]->SetYTitle("R_{AA}^{Pythia}");
  htmp1[r]->SetTitleOffset(1.2,"x");htmp1[r]->SetTitleOffset(1.1,"y");
  htmp1[r]->SetTitleSize(0.075,"x");htmp1[r]->SetTitleSize(0.08,"y");
  htmp1[r]->SetLabelSize(0.060,"x");htmp1[r]->SetLabelSize(0.060,"y");
  if(plotLogX_RAA)  htmp1[r]->GetXaxis()->SetMoreLogLabels(1);
  htmp1[r]->SetMinimum(yminRAA);htmp1[r]->SetMaximum(ymaxRAA);
  htmp1[r]->DrawCopy();
  
    //Krishna's RAA
	gKrishnaRAA[r]->SetLineColor(ckrsh);
	gKrishnaRAA[r]->SetFillStyle(krshFill);
	gKrishnaRAA[r]->SetFillColorAlpha(ckrsh,0.5);
	if(showKrishna) gKrishnaRAA[r]->Draw("2");
	
	if(showKrishnaAll)
	{	
		gKrishnaRAA1[r]->SetLineColor(ckrsh1);
		gKrishnaRAA1[r]->SetFillStyle(krshFill1);
		gKrishnaRAA1[r]->SetFillColorAlpha(ckrsh1,0.5);
		gKrishnaRAA1[r]->Draw("2");
		
		gKrishnaRAA2[r]->SetLineColor(ckrsh2);
		gKrishnaRAA2[r]->SetFillStyle(krshFill2);
		gKrishnaRAA2[r]->SetFillColorAlpha(ckrsh2,0.5);
		gKrishnaRAA2[r]->Draw("2");
	
		gKrishnaRAA3[r]->SetLineColor(ckrsh3);
		gKrishnaRAA3[r]->SetFillStyle(krshFill3);
		gKrishnaRAA3[r]->SetFillColorAlpha(ckrsh3,0.5);
		gKrishnaRAA3[r]->Draw("2");
	}
 
   //Ivans's RAA
  double tsize=0.038;
  if(showVitev) plot_g2(gVitRAA_g20[r],gVitRAA_g22[r],cvit1,lsvit1,lwvit1,"",0,-1,tsize);
  //if(showVitev) plot_g2(gVitRAA_g20b[r],gVitRAA_g22b[r],cvit2,lsvit2,lwvit2,"",0,-1,tsize);
  if(showVitev && r!=1) plot_g2(gVitRAA_NLO_down[r],gVitRAA_NLO_up[r],cvit3,lsvit3,lwvit3,"",0,-1,tsize);
  
   //LIDO
	gLIDORAA[r]->SetLineColor(clido);
	gLIDORAA[r]->SetFillStyle(lidoFill);
	gLIDORAA[r]->SetFillColorAlpha(clido,0.5);
	if(showLIDO) gLIDORAA[r]->Draw("3");
	
	//LBT
	gLBTRAA[r]->SetLineColor(clbt);
   gLBTRAA[r]->SetLineWidth(1);
   gLBTRAA[r]->SetMarkerStyle(marklbt);
	gLBTRAA[r]->SetMarkerSize(msizelbt);
	gLBTRAA[r]->SetMarkerColor(clbt);
	//gLBTRAA[r]->SetFillStyle(lbtFill);
	//gLBTRAA[r]->SetFillColor(clbt);
	if(showLBT) gLBTRAA[r]->Draw("P");
  
  
  //Phenix charged hadron RAA
	gphxRAAchhad[system]->SetLineColor(cphx);
   gphxRAAchhad[system]->SetLineWidth(1);
	gphxRAAchhad[system]->SetMarkerStyle(markphx);
	gphxRAAchhad[system]->SetMarkerSize(msizephx);
	gphxRAAchhad[system]->SetMarkerColor(cphx);
	gphxRAAchhad_sys[system]->SetLineColor(cphx);
   gphxRAAchhad_sys[system]->SetLineWidth(1);
	gphxRAAchhad_sys[system]->SetFillStyle(0);
	if(showPhenixhRAAchh) {
		gphxRAAchhad_sys[system]->Draw("[]");
		gphxRAAchhad[system]->Draw("P");
	}
	//Phenix pi0 RAA
	gphxRAApi0[system]->SetLineColor(cphx);
   gphxRAApi0[system]->SetLineWidth(1);
	gphxRAApi0[system]->SetMarkerStyle(markphx2);
	gphxRAApi0[system]->SetMarkerSize(msizephx);
	gphxRAApi0[system]->SetMarkerColor(cphx);
	gphxRAApi0_sys[system]->SetLineColor(cphx);
   gphxRAApi0_sys[system]->SetLineWidth(1);
	gphxRAApi0_sys[system]->SetFillStyle(0);
	if(showPhenixhRAApi0) {
		gphxRAApi0_sys[system]->Draw("[]");
		gphxRAApi0[system]->Draw("P");
	}
	
	//STAR hadron RAA
	gstrRAA[system]->SetLineWidth(1);
	gstrRAA[system]->SetLineColor(cstarH);
	gstrRAA[system]->SetMarkerStyle(markstarH);
	gstrRAA[system]->SetMarkerSize(msizestarH);
	gstrRAA[system]->SetMarkerColor(cstarH);
	if(showSTARRAAchh) gstrRAA[system]->Draw("P");
   
  //STAR RAA
  gjanRAA_sys[r]->SetLineWidth(1);
  gjanRAA_sys[r]->SetFillStyle(0);
  gjanRAA_sys[r]->SetLineColor(cstar);
  gjanRAA[r]->SetLineWidth(2);
  gjanRAA[r]->SetLineColor(cstar);
  gjanRAA[r]->SetMarkerStyle(markstar);
  gjanRAA[r]->SetMarkerSize(msizestar);
  gjanRAA[r]->SetMarkerColor(cstar);
  gjanRAA_sys_gray[r]->SetLineWidth(1);
  gjanRAA_sys_gray[r]->SetFillStyle(0);
  gjanRAA_sys_gray[r]->SetLineColor(cgray);
  gjanRAA_gray[r]->SetLineWidth(2);
  gjanRAA_gray[r]->SetLineColor(cgray);
  gjanRAA_gray[r]->SetMarkerStyle(markstar);
  gjanRAA_gray[r]->SetMarkerSize(msizestar);
  gjanRAA_gray[r]->SetMarkerColor(cgray);
  
  gjanRAA[r]->Draw("P");
  gjanRAA_sys[r]->Draw("2");
  if(drawBiasLine)
  {
		gjanRAA_gray[r]->Draw("P");
		gjanRAA_sys_gray[r]->Draw("2");
  }
  
  //ALICE RAA
  galiceRAA_sys->SetLineWidth(1);
  galiceRAA_sys->SetLineColor(calice);
  galiceRAA_sys->SetFillStyle(0);
  galiceRAA->SetLineWidth(2);
  galiceRAA->SetLineColor(calice);
  galiceRAA->SetMarkerStyle(markalice);
  galiceRAA->SetMarkerSize(msizealice);
  galiceRAA->SetMarkerColor(calice);
  if(plotAlice && showAliceRAA) galiceRAA->Draw("P");
  if(plotAlice && showAliceRAA) galiceRAA_sys->Draw("2");

 
	one->DrawClone("same");
	
	//normalization error
	if(showSTARRAAchh) ppboxStr->DrawClone("");
	if(showPhenixhRAAchh) ppboxPhx->DrawClone("");
	ppboxStrJet->DrawClone("");
  
  //LEGENDS
  if(r==0){
	  //general info
	posx1=0.25; posx2=0.6; posy1=0.65; posy2=0.9;
	if(system==peri){posx1=0.25;	posx2=0.6;	posy1=0.25;	posy2=0.45;}
	TLegend *model_info = new TLegend(posx1,posy1,posx2,posy2);
	model_info->SetTextSize(0.045);
	model_info->SetFillStyle(0);
	model_info->SetBorderSize(0);
	model_info->SetMargin(0.05);
	model_info->AddEntry("","Au+Au #sqrt{s_{NN}} = 200 GeV","");
	//model_info->AddEntry("", Form("Run11, %s","MB"), "");
	//model_info->AddEntry("", "Charged jets", "");
	model_info->AddEntry("", "Central (0-10%)", "");
	model_info->AddEntry("","anti-#it{k}_{T}", "");
	//model_info->AddEntry("", Form("#it{p}_{T}^{leading} > %.1lf GeV/#it{c}",pTlead), "");
	model_info->DrawClone("same");
	}
 	else if(r==1)
	{
  //legend 1
	posx1=0.36;	posx2=0.95;	posy1=0.58;	posy2=0.92;
	if(compareRAA==hadrons) posy1=0.63;
	else if(compareRAA==theory) posy1=0.65;
	if(system==peri){posx1=0.05;	posx2=0.5;	posy1=0.25;	posy2=0.45;}
	TLegend *legraa = new TLegend(posx1,posy1,posx2,posy2);
	legraa->SetTextSize(0.038);
	legraa->SetFillStyle(0);
	legraa->SetBorderSize(0);
	//legraa->AddEntry(gjanRAA[r], "","");
	legraa->AddEntry(gjanRAA[r], "STAR charged jets","lp");
	legraa->AddEntry(gjanRAA[r], Form("  #it{p}_{T, lead}^{min} = %.0lf GeV/#it{c}",pTlead),"");
	legraa->AddEntry(ppboxStrJet, "jet normalization unc.", "f");
	if(showAliceRAA)legraa->AddEntry(galiceRAA, "ALICE Pb+Pb (full j.)","lp");
	//legraa->AddEntry(gphxRAA, "PHENIX ch. hadrons ", "f");
	if(showSTARRAAchh)
	{
		legraa->AddEntry(gstrRAA[system], "STAR ch. hadrons (0-5%) ", "lp");
		legraa->AddEntry(ppboxStr, "STAR hadr. norm. unc.", "f");
	}
	if(compareRAA==all)
	{
		if(showPhenixhRAAchh) {
		legraa->AddEntry(gphxRAAchhad[system], "PHENIX ch. hadrons ", "lp");
		//legraa->AddEntry(ppboxPhx, "      hadron norm.unc.", "f");
		}	
		if(showPhenixhRAApi0) 
		legraa->AddEntry(gphxRAApi0[system], "PHENIX #pi^{0} ", "lp");
	}

	legraa->DrawClone("same");
	
	latexL->DrawLatex(0.25, 0.7,label);
	}
	else if(r==2)
	{
		//legend 2
	posx1=0.33;	posx2=0.9;	posy1=0.58;	posy2=0.93;
	if(compareRAA==theory) posy1=0.65;
	else if(compareRAA==hadrons) posy1=0.75;
	if(system==peri){posx1=0.05;	posx2=0.55;	posy1=0.25;	posy2=0.45;}
	TLegend *legraa2 = new TLegend(posx1,posy1,posx2,posy2);
	legraa2->SetTextSize(0.04);
	legraa2->SetFillStyle(0);
	legraa2->SetBorderSize(0);
	if(compareRAA==hadrons)
	{
		if(showPhenixhRAAchh) 
			legraa2->AddEntry(gphxRAAchhad[system], "PHENIX ch. hadrons ", "lp");
		if(showPhenixhRAApi0) 
			legraa2->AddEntry(gphxRAApi0[system], "PHENIX #pi^{0} ", "lp");
		if(showPhenixhRAAchh || showPhenixhRAApi0)
			legraa2->AddEntry(ppboxPhx, "PHENIX  hadr. norm. unc.", "f");

	}
	//if(showKrishna || showKrishnaAll || showLBT) legraa2->AddEntry("","charged jets:","");
	if(showLBT) legraa2->AddEntry(gLBTRAA[0], "LBT", "lp");
	if(showKrishna) legraa2->AddEntry(gKrishnaRAA[r], "Hybrid model", "f");
	if(showKrishnaAll) 
	{
		legraa2->AddEntry("","Hybrid model:","");
		legraa2->AddEntry(gKrishnaRAA1[r], "   no medium resp.", "f");
		legraa2->AddEntry(gKrishnaRAA2[r], "   pos. resp. from wake ", "f");
		legraa2->AddEntry(gKrishnaRAA3[r], "   full medium resp.", "f");
	}

		//if(showVitev) legraa2->AddEntry("","Full jets:","");
	if(showVitev) legraa2->AddEntry(gVitRAA_g20[r], "  SCET (full jets)", "l");
	//if(showVitev) legraa2->AddEntry(gVitRAA_g20b[r], "  SCET 2 ", "l");
	if(showVitev) legraa2->AddEntry(gVitRAA_NLO_down[r], "  NLO pQCD  (full jets)", "l");
	if(showLIDO) legraa2->AddEntry(gLIDORAA[0], "  LIDO  (full jets)", "f");
	
		
	legraa2->DrawClone("same");
	}
	

	latex->DrawLatex(Rxpos[r], 0.85,Form("#bf{R=%.1lf}",R)); //text is already bolded, #bf is used to unbold
	
	TLine *bias = new TLine(xbiasLine[r], yminRAA, xbiasLine[r], ybiasLine);
	bias->SetLineWidth(lineBiasWidth);
	bias->SetLineStyle(2);
	bias->SetLineColor(lineBiasColor);
	if(ppBias==0 && drawBiasLine) bias->DrawClone("same");
	TLatex *latexbias = new TLatex();
	latexbias->SetNDC();
	latexbias->SetTextSize(0.04);
	latexbias->SetTextColor(lineBiasColor);
	if(ppBias==0 && drawBiasLine) latexbias->DrawLatex(xbiasDesc[r], ybiasDesc,"--> ~ UNBIASED");
	
	}//R loop
	cfig1->SaveAs(Form("%s/RAA_%s_pTl%.0lf_bin%i%s%s.%s",outDir.Data(),C_system_name[system].Data(),pTlead,binning,sufPPbias[ppBias].Data(),comp_suff[compareRAA].Data(),ext.Data()));
  }
  if(pTlead>6) return;
  if(!showRCP) return;
  
	//********************
	//RCP
	//********************
	cout<<"plotting RCP"<<endl;
  
	int nR_RCP=2;
	TCanvas *cfig2=new TCanvas("cfig2","fig2",10,10,nR_RCP*860,700);
	cfig2->Divide(nR_RCP,1,0,0);
	
	for(int r=0; r<nR_RCP; r++)
	{

	float R=C_Rs[r];
	
	bool plotAliceRCP=(R<0.35) ? true : false; //we have Alice results only for R=0.2 and R=0.3
	float xmaxRCP_use=xmaxRCP;
	if(!plotAliceRCP) xmaxRCP_use=xmaxRCP_short;
	//Jan
	//fp[r]=new TFile(Form("AuAu/bin%i/systematics_R%.1lf_pT%0.lf.root",R,binning,pTlead),"read");
	TGraphAsymmErrors *gjanRCP_unf=(TGraphAsymmErrors*) fp[r]->Get("RCP_unfold_BSL");
	TGraphAsymmErrors *gjanRCP_corr=(TGraphAsymmErrors*) fp[r]->Get("RCP_corr_BSL");
	gjanRCP[r]=(TGraphAsymmErrors*) fp[r]->Get("RCP_BSL");
	ShrinkGraph(gjanRCP_unf,ptminRCP_STAR,ptmaxRCP_STAR);
	ShrinkGraph(gjanRCP_corr,ptminRCP_STAR,ptmaxRCP_STAR);
	ShrinkGraph(gjanRCP[r],ptminRCP_STAR,ptmaxRCP_STAR);
	gjanRCP_sys[r]=gcombine_syserr(gjanRCP_unf,gjanRCP_corr,1,0);

	//TGraphErrors* galiceRCP[r]=NULL;
	//TGraphAsymmErrors* galiceRCP[r]=NULL;

  //Draw plots
  //if(R>0.35) continue;
	cfig2->cd(r+1);
  gPad->SetMargin(0.15,0.03,0.20,0.03);
  if(r==0)gPad->SetRightMargin(0.0);
  if(r==1)
  {
	  gPad->SetLeftMargin(0.0);
	  //xminRCP=xminRCP+0.05;
  }
  gPad->SetTicks(1);
  if(showAliceRCP && plotLogX_RCP) gPad->SetLogx();
  if(plotLogY_RCP)gPad->SetLogy();
  htmp2[r]=new TH1D(Form("htmp2_%i",r),"",100,xminRCP,xmaxRCP_use);
  TString xtitle="";
  if(r==nR_RCP-1) xtitle="#it{p}_{T, jet}^{ch}, #it{p}_{T}^{ch}  (GeV/#it{c})  ";
  htmp2[r]->SetXTitle(xtitle);htmp2[r]->SetYTitle("R_{CP}");
  htmp2[r]->SetTitleOffset(1.05,"x");htmp2[r]->SetTitleOffset(0.95,"y");
  htmp2[r]->SetTitleSize(0.075,"x");htmp2[r]->SetTitleSize(0.080,"y");
  htmp2[r]->SetLabelSize(0.055,"x");htmp2[r]->SetLabelSize(0.055,"y");
  //htmp2->SetNdivisions(505,"x");htmp2->SetNdivisions(505,"y");
  htmp2[r]->SetMinimum(yminRCP);htmp2[r]->SetMaximum(ymaxRCP);
  htmp2[r]->DrawCopy();

	TLine *one = new TLine(xminRCP, 1, xmaxRCP,1);
	one->SetLineWidth(lineOneWidth);
	one->SetLineStyle(lineOneStyle);
	one->SetLineColor(lineOneColor);
	one->DrawClone("same");
  
    //ATLAS ch hadrons RCP
  gatlashRCP_sys->SetLineWidth(1);
  gatlashRCP_sys->SetLineColor(catlas);
  gatlashRCP_sys->SetFillStyle(0);
  gatlashRCP->SetLineWidth(1);
  gatlashRCP->SetLineColor(catlas);
  gatlashRCP->SetMarkerStyle(markatlasH);
  gatlashRCP->SetMarkerSize(msizeatlasH);
  gatlashRCP->SetMarkerColor(catlas);
  if(showAtlasRCP) gatlashRCP->Draw("P");
  if(showAtlasRCP) gatlashRCP_sys->Draw("2");
  
  //STAR ch. hadrons
  gSTARhRCP->SetLineWidth(1);
  gSTARhRCP->SetLineColor(cstarH);
  gSTARhRCP->SetMarkerStyle(markstarH);
  gSTARhRCP->SetMarkerSize(msizestarH);
  gSTARhRCP->SetMarkerColor(cstarH);
  if(showSTARhRCP) gSTARhRCP->Draw("P");
  
  //STAR recoil jets
  gIcp[r]->SetLineColor(calex);
  gIcp[r]->SetLineWidth(2);
  gIcp[r]->SetMarkerColor(calex);
  gIcp[r]->SetLineStyle(4);
  if(showAlex)gIcp[r]->Draw("l");
  gIcp_sys[r]->SetLineColor(calex);
  gIcp_sys[r]->SetLineWidth(2);
  gIcp_sys[r]->SetMarkerColor(calex);
  gIcp_sys[r]->SetLineStyle(3);
  if(showAlex)gIcp_sys[r]->Draw("l");
  
  //Alice inclusive jets
  galiceRCP_sys[r]->SetLineWidth(1);
  galiceRCP_sys[r]->SetLineColor(calice);
  galiceRCP_sys[r]->SetFillStyle(0);
  galiceRCP[r]->SetLineWidth(1);
  galiceRCP[r]->SetLineColor(calice);
  galiceRCP[r]->SetMarkerStyle(markalice);
  galiceRCP[r]->SetMarkerSize(msizealice);
  galiceRCP[r]->SetMarkerColor(calice);
  if(plotAliceRCP && showAliceRCP) galiceRCP[r]->Draw("P");
  if(plotAliceRCP && showAliceRCP) galiceRCP_sys[r]->Draw("2");

  //STAR inclusive jets  
    gjanRCP_sys[r]->SetLineWidth(1);
  gjanRCP_sys[r]->SetLineColor(cstar);
	gjanRCP_sys[r]->SetFillStyle(0);
  gjanRCP[r]->SetLineWidth(1);
  gjanRCP[r]->SetLineColor(cstar);
  gjanRCP[r]->SetMarkerStyle(markstar);
  gjanRCP[r]->SetMarkerSize(msizestar);
  gjanRCP[r]->SetMarkerColor(cstar);
	gjanRCP[r]->Draw("P");
  gjanRCP_sys[r]->Draw("2");
  
	if(r==0){
	infoXmin=0.7;
	infoXmax=0.9;
	infoYmin=(plotLogY_RCP)?0.25:0.75;
	infoYmax=(plotLogY_RCP)?0.4:0.9;
	TLegend *model_info = new TLegend(infoXmin,infoYmin,infoXmax,infoYmax);
	model_info->SetTextSize(RCP_legTextSize);
	model_info->SetFillStyle(0);
	model_info->SetBorderSize(0);
	model_info->SetMargin(0.05);
	//model_info->SetHeader("STAR Au+Au #sqrt{s_{NN}} = 200 GeV");
	//model_info->AddEntry("", Form("Run11, %s","MB"), "");
	//model_info->AddEntry("", "Charged jets", "");
	//model_info->AddEntry("", "0-10% / 60-80% central collisions", "");
	model_info->AddEntry("",Form("anti-#it{k}_{T}, R=%.1lf",R), "");
	model_info->AddEntry("", Form("#it{p}_{T, lead}^{min} = %.0lf GeV/#it{c}",pTlead), "");
	model_info->Draw("same");
	
	latexL->DrawLatex(0.55, 0.85,label);

	//legend
	posx1=0.18;
	posx2=0.55;
	posy1=(plotLogY_RCP)?0.25:0.65;
	posy2=(plotLogY_RCP)?0.5:0.9;
	if(showAlex && !plotLogY_RCP) posy1=0.55;
	TLegend *legrcp = new TLegend(posx1,posy1,posx2,posy2);
	legrcp->SetTextSize(RCP_legTextSize);
	legrcp->SetFillStyle(0);
	legrcp->SetBorderSize(0);
	//legrcp->AddEntry(gjanRCP[r], "STAR Au+Au","lp");
	legrcp->AddEntry(gjanRCP[r], "Au+Au #sqrt{s_{NN}} = 200 GeV","");
		//if(showAlex)legrcp->AddEntry(gjanRCP[r], " inclusive jets","");
	legrcp->AddEntry(gjanRCP[r], "  ch. jets 0-10% / 60-80%","lp");
		//legrcp->AddEntry(gjanRCP[r], "  0-10% / 60-80%","");
	if(showAlex)
	{
		//legrcp->AddEntry(gIcp[r], "STAR Au+Au","l");
		legrcp->AddEntry(gIcp[r], "  ch. recoil jets","l");
		legrcp->AddEntry(gIcp[r], "  0-10% / 60-80%","");
	}
	if(showSTARhRCP)
	{
		legrcp->AddEntry(gSTARhRCP,"  ch. hadrons 0-5% / 60-80%", "lp");
		//legrcp->AddEntry(gSTARhRCP,"  0-5% / 60-80%", "");
	}
	legrcp->Draw("same");

	}

	if(r==1)
	{
	infoXmin=0.75;
	infoXmax=0.9;
	infoYmin=(plotLogY_RCP)?0.25:0.8;
	infoYmax=(plotLogY_RCP)?0.4:0.9;
	TLegend *model_info2 = new TLegend(infoXmin,infoYmin,infoXmax,infoYmax);
	model_info2->SetTextSize(RCP_legTextSize);
	model_info2->SetFillStyle(0);
	model_info2->SetBorderSize(0);
	model_info2->SetMargin(0.05);
	model_info2->AddEntry("",Form("R=%.1lf",R), "");
	model_info2->Draw("same");
	
	posx1=0.05;
	posx2=0.5;
	posy1=(plotLogY_RCP)?0.25:0.65;
	posy2=(plotLogY_RCP)?0.5:0.9;
	TLegend *legrcp2 = new TLegend(posx1,posy1,posx2,posy2);
	legrcp2->SetTextSize(RCP_legTextSize);
	legrcp2->SetFillStyle(0);
	legrcp2->SetBorderSize(0);

	if(plotAliceRCP && showAliceRCP)	
	{
		legrcp2->AddEntry(galiceRCP[r], "Pb+Pb #sqrt{s_{NN}}=2.76 TeV","");
		legrcp2->AddEntry(galiceRCP[r], "  ch. jets 0-10% / 50-80%","lp");
		//legrcp2->AddEntry(galiceRCP[r], "  0-10% / 50-80% ","");
		legrcp2->AddEntry(gatlashRCP, "  ch. hadrons 0-5% / 60-80%","lp");
		//legrcp2->AddEntry(galiceRCP[r], "  0-10% / 50-80% ","");
	}
	legrcp2->Draw("same");
	}

	//latex->DrawLatex(Rxpos2[r], 0.75,Form("R=%.1lf",R));

	}//R loop

	cfig2->SaveAs(Form("%s/RCP_pTl%.0lf_bin%i%s.%s",outDir.Data(),pTlead,binning,sufPPbias[ppBias].Data(),ext.Data()));
  
}

//===============================================================
void ShrinkGraph(TGraphAsymmErrors* graph, double min, double max)
{
	for(int point=(graph->GetMaxSize()+1); point>=0; point--)
	{
		double xpoint,ypoint;
		graph->GetPoint(point, xpoint, ypoint);
		//cout<<point<<": x="<<xpoint<<endl;
		if(xpoint>max || xpoint<min) graph->RemovePoint(point);
	}
	return;
}
//===============================================================
void ShrinkGraph(TGraphErrors* graph, double min, double max)
{
	for(int point=(graph->GetMaxSize()+1); point>=0; point--)
	{
		double xpoint,ypoint;
		graph->GetPoint(point, xpoint, ypoint);
		//cout<<point<<": x="<<xpoint<<endl;
		if(xpoint>max || xpoint<min) graph->RemovePoint(point);
	}
	return;
}

//===============================================================

void plot_g2(TGraphErrors *g1,TGraphErrors *g2,int ic=2,int is=1,int iw=1,char *txt="",double xtxt=0.5,double ytxt=0.5,double tsiz=0.04){
  int n=g1->GetN()+g2->GetN()+1;
  //const int N=n;
  const int N=200;
  double xx[N],yy[N];
  double x,y;
  for(int i=0;i<g1->GetN();++i){
    g1->GetPoint(i,x,y);
    xx[i]=x;yy[i]=y;
  }
  for(int i=0;i<g2->GetN();++i){
    g2->GetPoint(i,x,y);
    xx[n-2-i]=x;yy[n-2-i]=y;
  }
  xx[n-1]=xx[0];yy[n-1]=yy[0];
  TGraph *g=new TGraph(n,xx,yy);
  g->SetLineColor(ic);g->SetLineStyle(is);g->SetLineWidth(iw);g->Draw("l");
  keyLine(xtxt,ytxt,txt,ic,is,tsiz,iw,1);
  return;
  /*
  g1->SetLineColor(ic);g1->SetLineStyle(is);g1->SetLineWidth(iw);g1->Draw("l");
  g2->SetLineColor(ic);g2->SetLineStyle(is);g2->SetLineWidth(iw);g2->Draw("l");
  TLine *line=new TLine();
  line->SetLineColor(ic);line->SetLineStyle(is);line->SetLineWidth(iw);
  double x1,y1,x2,y2;
  g1->GetPoint(0,x1,y1);g2->GetPoint(0,x2,y2);line->DrawLine(x1,y1,x2,y2);
  g1->GetPoint(g1->GetN()-1,x1,y1);g2->GetPoint(g2->GetN()-1,x2,y2);line->DrawLine(x1,y1,x2,y2);
  keyLine(xtxt,ytxt,txt,ic,is,tsiz,iw,1);*/
}

//===============================================================

//make a band from two graphs with same x values 
TGraphErrors* area_graph(TGraphErrors *g1,TGraphErrors *g2)
{
  int n=g1->GetN();
  //const int N=n;
  const int N=n;
  double xx[N],yy[N],yy_err[N];
  double x1,y1,x2,y2;
  for(int i=0;i<g1->GetN();++i){
    g1->GetPoint(i,x1,y1);
	 g2->GetPoint(i,x2,y2);
    xx[i]=x1;
	 yy[i]=y1+y2/2;
	 yy_err[i]=TMath::Abs(y2-y1)/2;
  }
  
  TGraphErrors *g=new TGraphErrors(n,xx,yy,0,yy_err);
  return g;
}

//===============================================================
TGraphAsymmErrors* gcombine_syserr(TGraphAsymmErrors *gr1,TGraphAsymmErrors *gr2,bool setErrX=1,bool RAA=1/*RAA or RCP*/)
{
	int npoints=gr1->GetN();
	const int n=30;
	double gx[n];
	double gx_l[n];
	double gx_h[n];
	double gy[n];
	double gy_l[n];
	double gy_h[n];
	
	for(int i=0;i<npoints;++i){
		double x1,y1;
		gr1->GetPoint(i,x1,y1);
		double x1_down;
		double x1_up;
		
		if(setErrX==0) //set x errors to 0
		x1_down=0; 
		x1_up=0; 
		else
		{
			x1_down=gr1->GetErrorXlow(i);
			x1_up=gr1->GetErrorXhigh(i);
		}
		double y1_down=gr1->GetErrorYlow(i);
		double y1_up=gr1->GetErrorYhigh(i);

		double y2_down=gr2->GetErrorYlow(i);
		double y2_up=gr2->GetErrorYhigh(i);
	
		double norm=(RAA)? C_RAA_NormErr[0] : C_RCP_NormErr; //relative error of the normalization

		gx[i]=x1;
		gx_l[i]=x1_down;
		gx_h[i]=x1_up;
		gy[i]=y1;
		gy_l[i]=TMath::Sqrt(y1_down*y1_down+y2_down*y2_down+y1*norm*y1*norm);
		gy_h[i]=TMath::Sqrt(y1_up*y1_up+y2_up*y2_up+y1*norm*y1*norm);
		
	}
	TGraphAsymmErrors* graph=new TGraphAsymmErrors(npoints, gx, gy, gx_l, gx_h, gy_l, gy_h);
	return graph;
		
}
//===============================================================

TGraph *graph_band(int n,double *x,double *y_high,double *y_low,double xmax=10){
  for(int i=0;i<n;++i)
  //const int nn=2*n+1;double xx[nn],yy[nn];
	  const int nn=50;double xx[nn],yy[nn];
  int N=0;
  for(int i=0;i<n;++i){
    if(x[i]>xmax)continue;
    xx[N]=x[i];yy[N]=y_high[i];
    N++;
  }
  for(int i=n;i<2*n;++i){
    if(x[2*n-1-i]>xmax)continue;
    xx[N]=x[2*n-1-i];yy[N]=y_low[2*n-1-i];
    N++;
  }
  xx[N]=xx[0];yy[N]=yy[0];
  TGraph *g=new TGraphErrors(N+1,xx,yy);
  return(g);
}
